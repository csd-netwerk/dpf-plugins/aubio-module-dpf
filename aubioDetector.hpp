#ifndef DISTRHO_PLUGIN_SLPLUGIN_HPP_INCLUDED
#define DISTRHO_PLUGIN_SLPLUGIN_HPP_INCLUDED

#include <cmath>

#include "DistrhoPlugin.hpp"
#include "aubio_module/src/aubio_module.hpp"
#include "aubio_module/src/aubio_onset.hpp"
#include "aubio_module/src/aubio_pitch.hpp"
#include "envFollower/envFollower.hpp"


START_NAMESPACE_DISTRHO

class AubioDetector : public Plugin
{
	public:
		enum Parameters
		{
            paramBuffersize,
            paramHopfactor,
            paramMethod,
			paramCount
		};

		AubioDetector();
		~AubioDetector();

	protected:
		//-------------------------------------------------------------------
		// Information

		const char* getLabel() const noexcept override
		{
			return "AubioDetector";
		}

		const char* getDescription() const override
		{
			return "Plugin that uses different Aubio algorithms for MIR detection";
		}

		const char* getMaker() const noexcept override
		{
			return "Geert Roks";
		}

		const char* getHomePage() const override
		{
			return "https://geertroks.com";
		}

		const char* getLicense() const noexcept override
		{
			return "MIT";
		}

		uint32_t getVersion() const noexcept override
		{
			return d_version(1, 0, 8);
		}

		int64_t getUniqueId() const noexcept override
		{
			return d_cconst('G', 'R', 'T', 's');
		}

//        void bufferSizeChanged(uint32_t newBufferSize);
//        void sampleRateChanged(double newSampleRate);

		// -------------------------------------------------------------------
		// Init

		void initParameter(uint32_t index, Parameter& parameter) override;
		void initProgramName(uint32_t index, String& programName) override;

		// -------------------------------------------------------------------
		// Internal data

		float getParameterValue(uint32_t index) const override;
		void  setParameterValue(uint32_t index, float value) override;
		void  loadProgram(uint32_t index) override;

		// -------------------------------------------------------------------
		// Process
		void activate() override;
		void deactivate() override;

        void midiNoteOn(float pitch, uint8_t velocity = 100);
        void midiNoteOff(float pitch);

		void run(const float** inputs, float** outputs, uint32_t frames, const MidiEvent* midiEvents, uint32_t midiEventCount) override;

	private:
		int bIndex = 0;
		double pSignal = 0.0;

        AubioModule* aubio;
        AubioPitch pitch_detector;
        AubioOnset onset_detector;
        EnvFollower envfollow;

        float playing_pitch = 0.0f;
        bool notePlaying = false;
        float prev_pitches[4];
        uint8_t prev_pitch_head = 0;

		void reset();

		DISTRHO_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(AubioDetector)
};

// -----------------------------------------------------------------------

END_NAMESPACE_DISTRHO

#endif  // DISTRHO_PLUGIN_SLPLUGIN_HPP_INCLUDED

