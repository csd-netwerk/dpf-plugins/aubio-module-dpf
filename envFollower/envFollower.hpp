#include <cmath>

class EnvFollower {
    public:
        EnvFollower();
        ~EnvFollower();

        float process(float in);

    private:
        float slider(float in);

        float prev_slideVal = 0.0f;
        float rise_time = 0.5f;
        float fall_time = 250.0f;
        float samplerate = 48000.0f;
};
