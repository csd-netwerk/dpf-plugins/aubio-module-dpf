#include <iostream>
#include "aubioDetector.hpp"

START_NAMESPACE_DISTRHO
// -----------------------------------------------------------------------

AubioDetector::AubioDetector()
	: Plugin(paramCount, 1, 0) // 1 program, 0 states
{
	// set default values
	loadProgram(0);

    this->aubio = &this->pitch_detector;
    this->aubio->setBuffersize(getBufferSize());
    this->aubio->setHopfactor(8);
    this->aubio->setSamplerate(getSampleRate());
    this->pitch_detector.setPitchMethod("yinfast");
    this->pitch_detector.setSilenceThreshold(-30.0f);
    this->pitch_detector.setPitchOutput("Midi");

	// reset
	reset();
}

AubioDetector::~AubioDetector()
{
}

// -----------------------------------------------------------------------
// Init

void AubioDetector::initParameter(uint32_t index, Parameter& parameter)
{
	switch (index)
	{
		case paramBuffersize:
			parameter.hints         = kParameterIsAutomable;
			parameter.name          = "Buffersize";
			parameter.symbol        = "B";
			parameter.unit          = "";
			parameter.ranges.def    = 6;
			parameter.ranges.min    = 1;
			parameter.ranges.max    = 6;
			break;
        case paramHopfactor:
            parameter.hints         = kParameterIsAutomable;
            parameter.name          = "Hopfactor";
            parameter.symbol        = "H";
            parameter.unit          = "";
            parameter.ranges.def    = 5;
            parameter.ranges.min    = 1;
            parameter.ranges.max    = 5;
            break;
        case paramMethod:
            parameter.hints         = kParameterIsAutomable;
            parameter.name          = "Method";
            parameter.symbol        = "M";
            parameter.unit          = "";
            parameter.ranges.def    = 6;
            parameter.ranges.min    = 1;
            parameter.ranges.max    = 6;
            break;
    }
}

void AubioDetector::initProgramName(uint32_t index, String& programName)
{
	if (index != 0)
		return;

	programName = "Default";
}

// -----------------------------------------------------------------------
// Internal data

float AubioDetector::getParameterValue(uint32_t index) const
{
    switch (index)
    {
        case paramBuffersize:
            return 0.0f;
        case paramHopfactor:
            return 0.0f;
        case paramMethod:
            return 0.0f;
    }
}

void AubioDetector::setParameterValue(uint32_t index, float value)
{
	switch (index)
	{
		case paramBuffersize:
			break;
        case paramHopfactor:
            break;
        case paramMethod:
            break;
	}
}

void AubioDetector::loadProgram(uint32_t index)
{
}

void AubioDetector::reset()
{
}

//void bufferSizeChanged(uint32_t newBufferSize) {
//    aubio.setBuffersize(newBufferSize);
//}
//
//void sampleRateChanged(double newSampleRate) {
//    aubio.setSamplerate(newSampleRate);
//}
//

// -----------------------------------------------------------------------
// Process

void AubioDetector::activate()
{
}

void AubioDetector::deactivate()
{
}

void AubioDetector::midiNoteOn(float pitch, uint8_t velocity) {
    MidiEvent event;

    event.frame = 0;
    event.size = 3;
    event.data[0] = 0x90;
    event.data[1] = pitch;
    event.data[2] = velocity;
    writeMidiEvent(event);
}

void AubioDetector::midiNoteOff(float pitch) {
    MidiEvent event;

    event.frame = 0;
    event.size = 3;
    event.data[0] = 0x80;
    event.data[1] = pitch;
    event.data[2] = 0;
    writeMidiEvent(event);
}


void AubioDetector::run(const float** inputs, float** outputs, uint32_t frames, const MidiEvent* midiEvents, uint32_t midiEventCount)
{
    const float* input  = inputs[0];
	float*       output = outputs[0];
    float envelope[frames];
    float env_sum = 0.0f;
    bool isOnset = false;
    bool isOffset = false;
    float confidence = 0.0f;

    // Main processing body
	for (uint32_t f = 0; f < frames; ++f)
	{
		// Clearing output buffer from random garbage (if not in-place processing)
		if (input != output) output[f] = 0.0f;

		output[f] = input[f];

        envelope[f] = envfollow.process(input[f]);
        env_sum += envelope[f];
	}
    env_sum = env_sum/frames;

    float detected_pitch = aubio->process((float*)input);
    confidence = pitch_detector.getPitchConfidence();
    detected_pitch = std::round(detected_pitch);
    //prev_pitches[prev_pitch_head] = detected_pitch;

    float env_acc = envelope[frames - 1] - envelope[0]; //Envelope acceleration

    //if (env_acc > 0.1f) {
    //    std::cout << "detected pitch: " << detected_pitch << ", env_acc: " << env_acc << std::endl;
    //}



    if (detected_pitch >= 38 && detected_pitch <= 90 && detected_pitch != playing_pitch) {
        if(confidence >= 0.85f) {
            // If the acceleration of the envelope is high, there is an onset
            isOnset = true;
        }
    }
    if (env_sum < 0.05f) {
        // If the envelope (check the middle sample of the buffer) dropped to low, there is an offset
        isOffset = true;
    }

    if (isOnset) {
        //std::cout << "is Onset" << std::endl;
        if (playing_pitch) {
            std::cout << "Midi Event: " << playing_pitch << " off" << std::endl;
            midiNoteOff(playing_pitch);
            std::cout << "Midi Event: " << detected_pitch << " on" << std::endl;
            midiNoteOn(detected_pitch, 100);
        } else {
            std::cout << "Midi Event: " << detected_pitch << " on" << std::endl;
            midiNoteOn(detected_pitch, 100);
        }
        playing_pitch = detected_pitch;
    } else if (isOffset && playing_pitch) {
        std::cout << "Midi Event: " << playing_pitch << " off" << std::endl;
        midiNoteOff(playing_pitch);
        playing_pitch = false;
    }

    //prev_pitch_head = prev_pitch_head % 4;
}

// -----------------------------------------------------------------------

Plugin* createPlugin()
{
	return new AubioDetector();
}

// -----------------------------------------------------------------------
END_NAMESPACE_DISTRHO
